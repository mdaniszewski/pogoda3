<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Article;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
Use App\Category;

class ArticlesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $articles = Article::paginate(15);

        return view('articles.articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {   
        $categories = Category::all();
        return view('articles.articles.create')->with('categories',$categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required', 'body' => 'required', 'category' =>'required', ]);
        $category = Category::first()->where('category',$request->category)->get();
       $article = new Article;
       $article->title=$request->title;
       $article->body=$request->body;
       foreach($category as $category){
        $article->category_id = $category->id;}
       $article->save();

        Session::flash('flash_message', 'Article added!');

        return redirect('articles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);

        return view('articles.articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id);

        return view('articles.articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $this->validate($request, ['title' => 'required', 'body' => 'required', ]);

        $article = Article::findOrFail($id);
        $article->update($request->all());

        Session::flash('flash_message', 'Article updated!');

        return redirect('articles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        Article::destroy($id);

        Session::flash('flash_message', 'Article deleted!');

        return redirect('articles');
    }

    public function wyswietl(){
        $articles = Article::orderBy('created_at','desc')->get();
        $categories = Category::all();
        $hot = Article::orderBy('created_at','desc')->take(5)->get();
        return View('the_news')->with('articles', $articles)->with('categories', $categories)->with('hot',$hot);
    }
    public function single($id){
        $article = Article::findOrFail($id);
        $categories = Category::all();
        $hot = Article::orderBy('created_at','desc')->get();
        return view('single')->with('article', $article)->with('categories',$categories)->with('hot',$hot);
    }
    public function cat($cat){
        $categories = Category::all();
        $category = Category::first()->where('category',$cat)->get();
        $hot = Article::orderBy('created_at','desc')->take(5)->get();
        foreach($category as $category)
            $id = $category->id;
        $articles = Article::where('category_id', $id)->get();
        return view('cat')->with('articles', $articles)->with('categories', $categories)->with('hot', $hot);
    }



}
