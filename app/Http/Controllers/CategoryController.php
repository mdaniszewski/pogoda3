<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category;
use Session;

class CategoryController extends Controller
{
    public function index()
    {
    	$categories = Category::paginate(15);
        return view('categoryList', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request, ['category' => 'required', ]);
        Category::create($request->all());
        return redirect('category');
    }

    public function create()
    {   
        return view('categoryCreate');
    }
     public function edit($id)
    {
        $category = Category::findOrFail($id);

        return view('categoryEdit', compact('category'));
    }
    public function update($id, Request $request)
    {
        $this->validate($request, ['category' => 'required', ]);

        $category = Category::findOrFail($id);
        $category->update($request->all());

        Session::flash('flash_message', 'Category updated!');

        return redirect('category');
    }
    public function destroy($id)
    {
        Category::destroy($id);

        Session::flash('flash_message', 'Category deleted!');

        return redirect('category');
    }


}
