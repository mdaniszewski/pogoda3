<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Condition;
use App\Article;
use App\Category;
use DateTime;
class ConditionController extends Controller
{
    public function show(Request $request){
    	$miasto = $request->input('szukaj');
        $conditions = Condition::where('city', $miasto)->get();
        $articles = Article::all()->take(3);
        $categories = Category::All();

        return View('home')->with('conditions', $conditions)->with('articles', $articles)->with('categories', $categories);
    }
    	
    public function showdef(){
    	$date = date('Y-m-d');
        $conditions = Condition::where('city','Olsztyn')->get();
        $categories = Category::all();
        $articles = Article::all()->take(3);

        return View('home')->with('conditions', $conditions)->with('articles', $articles)->with('categories', $categories);

    }
}
