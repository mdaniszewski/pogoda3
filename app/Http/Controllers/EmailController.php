<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Mail;
use Session;
use App\Category;
class EmailController extends Controller
{
    //
public function contact(){
    $categories = Category::all();
    return view('contact')->with('categories', $categories);

}

    public function send(Request $request){
        $this->validate($request, ['imie' => 'required', 'nazwisko' => 'required','content' => 'required', ]);
    	$title = 'Opinie o stronie';
    	$name = $request->input('imie');
    	$lname = $request->input('nazwisko');
    	$comp = $request->input('firma');
    	$site = $request->input('strona');
    	$content = $request->get('content');

    	Mail::send('send',['title' => $title, 'name' => $name, 'lname' => $lname, 'comp' => $comp, 'site' => $site, 'content' => $content], function($message){
            $message->to('daniszewski.michal@gmail.com','serwis pogodowy')->subject('Serwis pogodowy 404'); 

    	});
         session()->flash('flash_message', 'Wysłano wiadomość!');
		return view('contact');
	}
}
