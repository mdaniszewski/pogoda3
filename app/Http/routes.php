<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Support\Facades\Mail;



Route::get('/the_news', 'ArticlesController@wyswietl');
Route::get('contact', 'EmailController@contact');

Route::get('single/{id}', 'ArticlesController@single');
  Route::get('kategorie/{cat}', 'ArticlesController@cat');
  Route::get('kategorie/single/{id}', 'ArticlesController@single');
Route::auth();

Route::get('/admin', function()
    {
        return View('admin');
    });



Route::group(['middleware' => ['web']], function () {
	Route::resource('articles', 'ArticlesController');
    Route::resource('category', 'CategoryController');
    route::post('wiadomosc','EmailController@send');
    Route::get('/', 'ConditionController@showdef');
Route::get('main', 'ConditionController@showdef');
Route::post('/wyszukaj', 'ConditionController@show');
Route::get('/home', 'HomeController@index');

});