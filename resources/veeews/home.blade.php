@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

        <title>Serwis pogodowy</title>

        <!-- Loading third party fonts -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
        <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="js/OSC.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js"></script>


        <!-- Loading main css file -->
        <link rel="stylesheet" href="style.css">

        <!--[if lt IE 9]>
        <script src="js/ie-support/html5.js"></script>
        <script src="js/ie-support/respond.js"></script>
        <![endif]-->

    </head>


    <body>

        <div class="site-content">
            <div class="site-header">
                <div class="container">
                    <a href="main" class="branding">
                        <img src="images/logo.png" alt="" class="logo">
                        <div class="logo-type">
                            <h1 class="site-title">Serwis pogodowy</h1>
                            <small class="site-description">Pogoda w Twoim mieście</small>
                        </div>
                    </a>

                    <!-- Default snippet for navigation -->
                    <div class="main-navigation">
                        <button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
                        <ul class="menu">
                            <li class="menu-item current-menu-item"><a href="main">Prognoza</a></li>
                            <li class="menu-item"><a href="the_news">Wiadomości</a></li>
                            <li class="menu-item"><a href="contact">Kontakt</a></li>
                        </ul> <!-- .menu -->
                    </div> <!-- .main-navigation -->

                    <div class="mobile-navigation"></div>

                </div>
            </div> <!-- .site-header -->

            <div class="forecast-table" >

                <div class="container">

                    <form action="#" class="find-location">
                        <input type="text" placeholder="Znajdź swoją lokalizację...">
                        <input type="submit" value="Szukaj">
                    </form>


                    <div class="forecast-container">
                        <div class="today forecast">
                            <div class="forecast-header">
                                <div class="day">Poniedziałek</div>
                                <div class="date">6 Maj</div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="location">Olsztyn</div>
                                <div class="degree">
                                    <div class="num">23<sup>o</sup>C</div>
                                    <div class="forecast-icon">
                                        <img src="images/icons/icon-1.svg" alt="" width=90>
                                    </div>
                                </div>
                                <span><img src="images/icon-umberella.png" alt="">20%</span>
                                <span><img src="images/icon-wind.png" alt="">18km/h</span>
                                <span><img src="images/icon-compass.png" alt="">East</span>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day">Wtorek</div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <img src="images/icons/icon-3.svg" alt="" width=48>
                                </div>
                                <div class="degree">23<sup>o</sup>C</div>
                                <small>18<sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day">Środa</div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <img src="images/icons/icon-5.svg" alt="" width=48>
                                </div>
                                <div class="degree">23<sup>o</sup>C</div>
                                <small>18<sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day">Czwartek</div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <img src="images/icons/icon-7.svg" alt="" width=48>
                                </div>
                                <div class="degree">23<sup>o</sup>C</div>
                                <small>18<sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day">Piątek</div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <img src="images/icons/icon-12.svg" alt="" width=48>
                                </div>
                                <div class="degree">23<sup>o</sup>C</div>
                                <small>18<sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day">Sobota</div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <img src="images/icons/icon-13.svg" alt="" width=48>
                                </div>
                                <div class="degree">23<sup>o</sup>C</div>
                                <small>18<sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day">Niedziela</div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <img src="images/icons/icon-14.svg" alt="" width=48>
                                </div>
                                <div class="degree">23<sup>o</sup>C</div>
                                <small>18<sup>o</sup></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <main class="main-content">


                 <div id='status'></div>
    <div id='map'></div>

    <script>
      if (typeof navigator.geolocation == 'undefined')
         alert("Funkcja geolokacji nie jest obsĹugiwana.")
      else
        navigator.geolocation.getCurrentPosition(granted, denied)

      function granted(position)
      {
        // UsuĹ poniĹźsze komentarze, aby wyĹwietliÄ lokalizacjÄ
        //    alert("JesteĹ tutaj:\n"
        //       + position.coords.latitude + ", "
        //       + position.coords.longitude)
 
        O('status').innerHTML = 'Pozwolenie przyznane'
        S('map').border       = '1px solid black'
        S('map').width        = '60%'
        S('map').height       = '700px'
        S('map').margin       = 'auto'
        O('status').margin = '0'
        var lat   = position.coords.latitude
        var long  = position.coords.longitude
        var gmap  = O('map')
        var myLatLng = {lat: lat, lng: long};
 
        var gopts =
        {
          center: new google.maps.LatLng(lat, long),
          zoom: 9, mapTypeId: google.maps.MapTypeId.ROADMAP
 
        }
        var map = new google.maps.Map(gmap, gopts)
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title:'Ok'
        });
      }

      function denied(error)
      {
        var message

        switch(error.code)
        {
          case 1: message = 'Brak pozwolenia'; break;
          case 2: message = 'PoĹoĹźenie niedostÄpne'; break;
          case 3: message = 'Przekroczony czas operacji'; break;
          case 4: message = 'Nieznany bĹÄd'; break;
        }

        // UsuĹ poniĹźsze komentarze, aby wyĹwietliÄ lokalizacjÄ
        //   alert("BĹÄd w ustalaniu poĹoĹźenia: " + message)

        O('status').innerHTML = message
      }
    </script>
                </div>

                <div class="fullwidth-block" data-bg-color="#4c4e5a">
                    <div class="container">
                    <h2 class="section-title">Najnowsze wiadomości</h2>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="news">

                                    <div class="photo-grid">
                                    <a href="#"><img src="images/thumb-1.jpg" alt="#"></a>
                                    </div>
                                    <h3><a href="#">Doloremque laudantium totam sequi </a></h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo saepe assumenda dolorem modi, expedita voluptatum ducimus necessitatibus. Asperiores quod reprehenderit necessitatibus harum, mollitia, odit et consequatur maxime nisi amet doloremque.</p>
                                    <small class="date">8 oct, 8:00AM</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="news">
                                    <div class="photo-grid">
                                    <a href="#"><img src="images/thumb-1.jpg" alt="#"></a>
                                    </div>
                                    <h3><a href="#">Doloremque laudantium totam sequi </a></h3>
                                    <p>Nobis architecto consequatur ab, ea, eum autem aperiam accusantium placeat vitae facere explicabo temporibus minus distinctio cum optio quis, dignissimos eius aspernatur fuga. Praesentium totam, corrupti beatae amet expedita veritatis.</p>
                                    <small class="date">8 oct, 8:00AM</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="news">
                                    <div class="photo-grid">
                                    <a href="#"><img src="images/thumb-1.jpg" alt="#"></a>
                                    </div>
                                    <h3><a href="#">Doloremque laudantium totam sequi </a></h3>
                                    <p>Enim impedit officiis placeat qui recusandae doloremque possimus, iusto blanditiis, quam optio delectus maiores. Possimus rerum, velit cum natus eos. Cumque pariatur beatae asperiores, esse libero quas ad dolorem. Voluptates.</p>
                                    <small class="date">8 oct, 8:00AM</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <h2 class="section-title">Video</h2>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="live-camera">
                                    <figure class="live-camera-cover"><img src="images/live-camera-1.jpg" alt=""></figure>
                                    <h3 ><a href="#">Doloremque laudantium totam sequi </a></h3>
                                    <small class="date">8 oct, 8:00AM</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="live-camera">
                                    <figure class="live-camera-cover"><img src="images/live-camera-2.jpg" alt=""></figure>
                                    <h3 ><a href="#">Doloremque laudantium totam sequi </a></h3>
                                    <small class="date">8 oct, 8:00AM</small>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="live-camera">
                                    <figure class="live-camera-cover"><img src="images/live-camera-3.jpg" alt=""></figure>
                                    <h3><a href="#">Doloremque laudantium totam sequi </a></h3>
                                    <small class="date">8 oct, 8:00AM</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </main> <!-- .main-content -->

            <footer class="site-footer">
            <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <h2 class="section-title">Mapa serwisu</h2>
                                <ul class="arrow-list">
                                    <li><a href="main">Prognoza</a></li>
                                    <li><a href="the_news">Wiadomości</a></li>
                                    <li><a href="contact">Kontakt</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h2 class="section-title">Wiadomości</h2>
                                <ul class="arrow-list">
                                    <li><a href="#">Z Polski</a></li>
                                    <li><a href="#">Ze świata</a></li>
                                    <li><a href="#">Dla alergików</a></li>
                                    <li><a href="#">Dla kierowców</a></li>
                                    <li><a href="#">Dla rolników</a></li>
                                    <li><a href="#">Dla turystów</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h2 class="section-title"></h2>
                                <form action="#" class="subscribe-form">
                                <input type="text" placeholder="Wpisz swój mail...">
                                <input type="submit" value="Subskrybuj">
                            </form>

                            </div>
                            <div class="col-md-3 col-md-offset-1">
                            <div class="social-links">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                <div class="container">
                    <p class="colophon">Copyright 2016 Zespół 404. All rights reserved</p>
                </div>
            </footer> <!-- .site-footer -->
        </div>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

    </body>

</html>
@endsection
