<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

    <style>
        body {
            font-family: 'Lato';
        }

        .fa-btn {
            margin-right: 6px;
        }
    </style>
</head>
<body style="background-color: white; border: 0px;" id="app-layout">

    <nav style="background-color: #1e202b; border: 0px;" class="navbar navbar-default navbar-static-top">
        <div class="container">

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
               
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="color:white;">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>

   

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>
<div style="margin-left:10%; margin-right: 10%;">
<p style="font-size: 17px; padding-bottom:10px;" class="uxp-align-center wysiwyg-font-type-verdana"><span style="color: rgb(51, 51, 51);">Lista artykułów</span><a href="{{ url('articles/create') }}" class="btn btn-primary" role="button" style="margin-left:12px;">Dodaj nowy</a>
</p>
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="margin:0px 15px 15px 0px;">Modyfikuj
  <span class="caret"></span></button>
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="margin:0px 15px 15px 0px;">Kategorie
  <span class="caret"></span></button>
   <table class="table table-striped">
    <thead style="background-color:#3498DB; color:white;">
      <tr>
        <th></th>
        <th>Tytuł</th>
        <th>Kategoria</th>
        <th>Data</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td><input type="checkbox"></input></td>
        <td>Lorem ipsum dolor sit amet.</td>
        <td>z Polski</td>
        <td>10.04.2016</td>
      </tr>
      <tr>
        <td><input type="checkbox"></input></td>
        <td>Aliquam at porttitor sem.</td>
        <td>dla kierowców</td>
        <td>10.03.2016</td>
      </tr>
      <tr>
        <td><input type="checkbox"></input></td>
        <td>Donec placerat nisl magna, et faucibus arcu.</td>
        <td>dla alergików</td>
        <td>01.03.</td>
      </tr>
    </tbody>
    </table>
    <div class="pull-right pagination"><ul class="pagination"><li class="page-pre"><a href="javascript:void(0)">‹</a></li><li class="page-number active"><a href="javascript:void(0)">1</a></li><li class="page-number"><a href="javascript:void(0)">2</a></li><li class="page-number"><a href="javascript:void(0)">3</a></li><li class="page-number"><a href="javascript:void(0)">4</a></li><li class="page-number"><a href="javascript:void(0)">5</a></li><li class="page-last-separator disabled"><a href="javascript:void(0)">...</a></li><li class="page-last"><a href="javascript:void(0)">7</a></li><li class="page-next"><a href="javascript:void(0)">›</a></li></ul></div>
</div>

