@extends('layouts.master')

@section('content')

    <h1>Artykuły<a href="{{ url('articles/create') }}" class="btn btn-primary pull-right btn-sm">Dodaj Artykuł</a> <a href="{{ url('category') }}" class="btn btn-primary pull-right btn-sm" style="margin-right:10px;">Kategorie</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Id</th><th>Tytuł</th><th>Kategoria</th><th>Działania</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($articles as $item)
                {{-- */$x++;/* --}}
                <tr style="max-height:78px !important; overflow:hidden; text-overflow:ellipsis;">
                    <td>{{ $x }}</td>
                    <td><a href="{{ url('articles', $item->id) }}">{{ $item->title }}</a></td>
                    <td><a href="{{ url('articles', $item->id) }}">{{ $item->category->category }}</a></td>

                    <td>
                        <a href="{{ url('articles/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">Edytuj</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['articles', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Usuń', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $articles->render() !!} </div>
    </div>

@endsection
