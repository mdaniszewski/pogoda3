@extends('layouts.master')

@section('content')

    <h1>Artykuły</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID</th> <th>Tytuł</th><th>Treść</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $article->id }}</td> <td> {{ $article->title }} </td><td> {{ $article->body }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection