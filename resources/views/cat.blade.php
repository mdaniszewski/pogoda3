<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">
		
		<title>Serwis pogodowy</title>

		<!-- Loading third party fonts -->
		<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
		<link href="{{URL::asset('fonts/font-awesome.min.css')}}" rel="stylesheet" type="text/css">


		<!-- Loading main css file -->
		<link rel="stylesheet" href="{{ URL::asset('style.css') }}">
		
		<!--[if lt IE 9]>
		<script src="js/ie-support/html5.js"></script>
		<script src="js/ie-support/respond.js"></script>
		<![endif]-->

	</head>


	<body>
		
		<div class="site-content">
			<div class="site-header">
				<div class="container">
					<a href="{{ URL::asset('main') }}" class="branding">
						{{HTML::image('images/logo.png')}}
						<div class="logo-type">
							<h1 class="site-title">Serwis pogodowy</h1>
							<small class="site-description">Pogoda w Twoim mieście</small>
						</div>
					</a>

					<!-- Default snippet for navigation -->
					<div class="main-navigation">
						<button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
						<ul class="menu">
							<li class="menu-item"><a href="{{ URL::asset('main') }}">Prognoza</a></li>
							<li class="menu-item current-menu-item"><a href="{{ URL::asset('the_news') }}">Wiadomości</a></li>
							<li class="menu-item"><a href="{{ URL::asset('contact') }}">Kontakt</a></li>
						</ul> <!-- .menu -->
					</div> <!-- .main-navigation -->

					<div class="mobile-navigation"></div>

				</div>
			</div> <!-- .site-header -->

			<main class="main-content">
				<div class="container">
					<div class="breadcrumb">
						<a href="{{ URL::asset('main') }}">Główna</a>
						<a href="{{ URL::asset('the_news') }}">Wiadomości</a>
						<span>Z Polski</span>
					</div>
				</div>
				

				<div class="fullwidth-block">
					<div class="container">
						<div class="row">
							<div class="content col-md-8">
								@foreach($articles as $article)
       	 							<div class="post">
									<h2 class="entry-title"> {{ $article->title }} </h2>
									<?php
									$i = rand(1,3);?>
									<div class="featured-image">{{HTML::image('images/featured-image-'.$i.'.jpg ')}}</div>
									<p style=" max-height:78px; width:750px; "> 
									<?php
									$text = $article->body;
									$maxPos = 500;
									if(strlen($text) > $maxPos){
										$lastPos = ($maxPos -3)-strlen($text);
										$text = substr($text,0,strrpos($text,' ',$lastPos)).'...';
									}echo $text;
									?>
									</p>
									<a href="single/{{$article->id}}" class="button">Czytaj więcej</a>
								</div>
    							@endforeach
							</div>
							<div class="sidebar col-md-3 col-md-offset-1">
								<div class="widget">
									<h3 class="widget-title">Najnowsze</h3>
									<ul class="arrow-list">
										@foreach($hot as $hot)
										<li><a href="single/{{$hot->id}}">{{$hot->title}}</a></li>
									@endforeach
									</ul>
								</div>

								

								<div class="widget top-rated">
									<h3 class="widget-title">Najpopularniejsze</h3>
									<ul>
										<li><h3 class="entry-title"><a href="#">Doloremque laudantium lorem</a></h3><div class="rating"><strong>5.5</strong> (759 rates)</div></li>
										<li><h3 class="entry-title"><a href="#">Doloremque laudantium lorem</a></h3><div class="rating"><strong>5.5</strong> (759 rates)</div></li>
										<li><h3 class="entry-title"><a href="#">Doloremque laudantium lorem</a></h3><div class="rating"><strong>5.5</strong> (759 rates)</div></li>
										<li><h3 class="entry-title"><a href="#">Doloremque laudantium lorem</a></h3><div class="rating"><strong>5.5</strong> (759 rates)</div></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</main> <!-- .main-content -->

			<footer class="site-footer">
			<div class="container">
						<div class="row">
							<div class="col-md-4">
								<h2 class="section-title">Mapa serwisu</h2>
								<ul class="arrow-list">
									<li><a href="{{ URL::asset('main') }}">Prognoza</a></li>
									<li><a href="{{ URL::asset('the_news') }}">Wiadomości</a></li>
									<li><a href="{{ URL::asset('contact') }}">Kontakt</a></li>
								</ul>
							</div>
							<div class="col-md-4">
								<h2 class="section-title">Wiadomości</h2>
								<ul class="arrow-list">
									@foreach($categories as $category)
										<li><a href="kategorie/{{$category->category}}">{{$category->category}}</a></li>
									@endforeach
								</ul>
							</div>
							<div class="col-md-4">
								<h2 class="section-title"></h2>
								<form action="#" class="subscribe-form">
								<input type="text" placeholder="Wpisz swój mail...">
								<input type="submit" value="Subskrybuj">
							</form>

							</div>
							<div class="col-md-3 col-md-offset-1">
							<div class="social-links">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
								<a href="#"><i class="fa fa-pinterest"></i></a>
							</div>
						</div>
						</div>
					</div>
				<div class="container">
					<p class="colophon">Copyright 2016 Zespół 404. All rights reserved</p>
				</div>
			</footer> <!-- .site-footer -->
		</div>
		
		<script src="js/jquery-1.11.1.min.js"></script>
		<script src="js/plugins.js"></script>
		<script src="js/app.js"></script>
		
	</body>

</html>