@extends('layouts.master')

@section('content')

    <h1>Kategorie<a href="{{ url('category/create') }}" class="btn btn-primary pull-right btn-sm">Dodaj Kategorię</a> <a href="{{ url('articles') }}" class="btn btn-primary pull-right btn-sm" style="margin-right:10px;">Artykuły</a></h1>
    <div class="table">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>Id</th><th>Kategoria</th><th>Działania</th>
                </tr>
            </thead>
            <tbody>
            {{-- */$x=0;/* --}}
            @foreach($categories as $item)
                {{-- */$x++;/* --}}
                <tr style="max-height:78px !important; overflow:hidden; text-overflow:ellipsis;">

                    <td><a href="{{ url('categories', $item->id) }}">{{ $item->id }}</a></td>
                    <td><a href="{{ url('categories', $item->id) }}">{{ $item->category }}</a></td>

                    <td>
                        <a href="{{ url('category/' . $item->id . '/edit') }}">
                            <button type="submit" class="btn btn-primary btn-xs">Edytuj</button>
                        </a> /
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['category', $item->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::submit('Usuń', ['class' => 'btn btn-danger btn-xs']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="pagination"> {!! $categories->render() !!} </div>
    </div>

@endsection
