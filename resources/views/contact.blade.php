<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

        <title>Serwis pogodowy - Zespół 404</title>

        <!-- Loading third party fonts -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
        <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">

        <!-- Loading main css file -->
        <link rel="stylesheet" href="style.css">

        <!--[if lt IE 9]>
        <script src="js/ie-support/html5.js"></script>
        <script src="js/ie-support/respond.js"></script>
        <![endif]-->

    </head>


    <body>

        <div class="site-content">
            <div class="site-header">
                <div class="container">
                    <a href="main" class="branding">
                        <img src="images/logo.png" alt="" class="logo">
                        <div class="logo-type">
                            <h1 class="site-title">Company name</h1>
                            <small class="site-description">tagline goes here</small>
                        </div>
                    </a>

                    <!-- Default snippet for navigation -->
                    <div class="main-navigation">
                        <button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
                        <ul class="menu">
                            <li class="menu-item"><a href="main">Główna</a></li>
                            <li class="menu-item"><a href="the_news">Wiadomości</a></li>
                            <li class="menu-item current-menu-item"><a href="contact">Kontakt</a></li>
                        </ul> <!-- .menu -->
                    </div> <!-- .main-navigation -->

                    <div class="mobile-navigation"></div>

                </div>
            </div> <!-- .site-header -->

            <main class="main-content">
                <div class="container">
                    <div class="breadcrumb">
                        <a href="index.html">Strona Główna</a>
                        <span>Kontakt</span>
                    </div>
                </div>

                <div class="fullwidth-block">
                    <div class="container">
                        <div class="col-md-5">
                            <div class="contact-details">
                                <div class="map" data-latitude="53.7440253" data-longitude="20.4539037"></div>
                                <div class="contact-info">
                                    <address>
                                        <img src="images/icon-marker.png" alt="">
                                        <p>Projekt 404 <br>
                                        Słoneczna 54, Olsztyn</p>
                                    </address>

                                    <a href="#"><img src="images/icon-phone.png" alt="">+48 800 800 800</a>
                                    <a href="#"><img src="images/icon-envelope.png" alt="">kontakt@pogoda404.com</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-md-offset-1">
                            <h2 class="section-title">Napisz do Nas!</h2>
                            <p>Wiesz o niecodziennim zjawisku atmosferycznym, a może chcesz się podzielić zdjęciami które ostatnio zrobiłeś? Napisz do nas a my je opublikujemy!</p>
                            <form action="wiadomosc" method="post" class="contact-form">
                                <div class="row">
                                    <div class="col-md-6"><input type="text" placeholder="Imię" name="imie" required></div>
                                    <div class="col-md-6"><input type="text" placeholder="Nazwisko" name="nazwisko" required></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6"><input type="text" placeholder="Firma" name="firma"></div>
                                    <div class="col-md-6"><input type="text" placeholder="Strona WWW" name="strona"></div>
                                </div>
                                <textarea placeholder="Wiadomość" name="content" required></textarea>
                                <div class="text-right">
                                    <button type="submit" placeholder="Wyślij" name="wyslij"></a>Wyślij</button>
                                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                                </div>
                            </form>


                             @if(Session::has('flash_message'))
                    {{Session::get('flash_message')}}
                @endif

                        </div>
                    </div>
                </div>

            </main> <!-- .main-content -->

            <footer class="site-footer">
			<div class="container">
						<div class="row">
							<div class="col-md-4">
								<h2 class="section-title">Mapa serwisu</h2>
								<ul class="arrow-list">
									<li><a href="main">Prognoza</a></li>
									<li><a href="the_news">Wiadomości</a></li>
									<li><a href="contact">Kontakt</a></li>
								</ul>
							</div>
							<div class="col-md-4">
								<h2 class="section-title">Wiadomości</h2>
								<ul class="arrow-list">
									@foreach($categories as $category)
                                        <li><a href="kategorie/{{$category->category}}">{{$category->category}}</a></li>
                                    @endforeach
								</ul>
							</div>
							<div class="col-md-4">
								<h2 class="section-title"></h2>
								<form action="#" class="subscribe-form">
								<input type="text" placeholder="Wpisz swój mail...">
								<input type="submit" value="Subskrybuj">
							</form>

							</div>
							<div class="col-md-3 col-md-offset-1">
							<div class="social-links">
								<a href="#"><i class="fa fa-facebook"></i></a>
								<a href="#"><i class="fa fa-twitter"></i></a>
								<a href="#"><i class="fa fa-google-plus"></i></a>
								<a href="#"><i class="fa fa-pinterest"></i></a>
							</div>
						</div>
						</div>
					</div>
				<div class="container">
					<p class="colophon">Copyright 2016 Zespół 404. All rights reserved</p>
				</div>
			</footer> <!-- .site-footer -->
        </div>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

    </body>

</html>
