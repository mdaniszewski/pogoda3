

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1">

        <title>Serwis pogodowy</title>

        <!-- Loading third party fonts -->
        <link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet" type="text/css">
        <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="js/OSC.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js"></script>


        <!-- Loading main css file -->
        <link rel="stylesheet" href="style.css">

        <!--[if lt IE 9]>
        <script src="js/ie-support/html5.js"></script>
        <script src="js/ie-support/respond.js"></script>
        <![endif]-->

    </head>


    <body>

        <div class="site-content">
            <div class="site-header">
                <div class="container">
                    <a href="main" class="branding">
                        <img src="images/logo.png" alt="" class="logo">
                        <div class="logo-type">
                            <h1 class="site-title">Serwis pogodowy</h1>
                            <small class="site-description">Pogoda w Twoim mieście</small>
                        </div>
                    </a>

                    <!-- Default snippet for navigation -->
                    <div class="main-navigation">
                        <button type="button" class="menu-toggle"><i class="fa fa-bars"></i></button>
                        <ul class="menu">
                        @if(isset($_COOKIE['dada']))
                            @if (Auth::guest())
                            <li class="menu-item"><a href="{{ url('/login') }}">Zaloguj</a></li>
                            @else
                            <li class="menu-item"><a href="{{ url('/logout') }}">Wyloguj</a></li>
                            <li class="menu-item"><a href="{{ url('/articles') }}">Panel admina</a></li>
                            @endif
                        @endif
                            <li class="menu-item current-menu-item"><a href="main">Prognoza</a></li>
                            <li class="menu-item"><a href="the_news">Wiadomości</a></li>
                            <li class="menu-item"><a href="contact">Kontakt</a></li>
                           
                        </ul> <!-- .menu -->
                    </div> <!-- .main-navigation -->

                    <div class="mobile-navigation"></div>

                </div>
            </div> <!-- .site-header -->

            <div class="forecast-table" >

                <div class="container">

                    <form action="wyszukaj" class="find-location" id="location" method="post">
                        <input type="text" placeholder="Znajdź swoją lokalizację..." name="szukaj">
                        <input type="submit" value="Szukaj">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                    </form>
                    


                    <div class="forecast-container">
                        <div class="today forecast">
                            <div class="forecast-header">
                                <div class="day"><?php
                                    $dw = date('w',strtotime('+0 day'));
                                            switch ($dw) {
                                                case 0:
                                                echo "Niedziela";
                                                break;
                                                case 1:
                                                echo "Poniedziałek";
                                                break;
                                                case 2:
                                                echo "Wtorek";
                                                break;
                                                case 3:
                                                echo "Środa";
                                                break;
                                                case 4:
                                                echo "Czwartek";
                                                break;
                                                case 5:
                                                echo "Piątek";
                                                break;
                                                case 6:
                                                echo "Sobota";
                                                break;
                                        }?></div>
                                <div class="date"><?php
                                    $date = date('Y-m-d',strtotime('+0 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo $condition->date;
                                    }?></div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="num"><div class="degree"><?php
                                    $date = date('Y-m-d',strtotime('+0 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo $condition->city;
                                    }?></div></div>
                                <div class="degree">
                                    <div class="num" style="font-size:24px; padding-bottom:27px;"><?php
                                    $date = date('Y-m-d',strtotime('+0 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date){
                                            echo '2:00 - 8:00 '.$condition->temp_morning.'<sup>o</sup>C</br>'.'8:00 - 14:00 '.$condition->temp_midday.'<sup>o</sup>C</br>'.'14:00 - 20:00 '.$condition->temp_evening;
                                        }
                                    }?><sup>o</sup>C</div>
                                    <div class="forecast-icon" style="margin-top:-50px;">
                                        <?php
                                    $date = date('Y-m-d',strtotime('+0 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo'<img src="images/icons/'.$condition->overall.'.svg" alt="" width=90>';
                                    }?>
                                    </div>
                                </div>
                                <span><img src="images/icon-umberella.png" alt=""><?php
                                    $date = date('Y-m-d',strtotime('+0 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo $condition->rain;
                                    }?>%</span>
                                <span><img src="images/icon-wind.png" alt=""><?php
                                    $date = date('Y-m-d',strtotime('+0 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo $condition->windspd;
                                    }?></span>
                                <span><img src="images/icon-compass.png" alt=""><?php
                                    $date = date('Y-m-d',strtotime('+0 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo $condition->winddir;
                                    }?></span>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day"><?php
                                    $dw = date('w',strtotime('+1 day'));
                                            switch ($dw) {
                                                case 0:
                                                echo "Niedziela";
                                                break;
                                                case 1:
                                                echo "Poniedziałek";
                                                break;
                                                case 2:
                                                echo "Wtorek";
                                                break;
                                                case 3:
                                                echo "Środa";
                                                break;
                                                case 4:
                                                echo "Czwartek";
                                                break;
                                                case 5:
                                                echo "Piątek";
                                                break;
                                                case 6:
                                                echo "Sobota";
                                                break;
                                        }?></div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <?php
                                    $date = date('Y-m-d',strtotime('+1 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo'<img src="images/icons/'.$condition->overall.'.svg" alt="" width=48>';
                                    }?>
                                </div>
                                <div class="degree"><?php
                                    $date = date('Y-m-d',strtotime('+1 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmax;
                                    }
                                    ?><sup>o</sup>C</div>
                                <small><?php
                                    $date = date('Y-m-d',strtotime('+1 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmin;
                                    }
                                    ?><sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day"><?php
                                    $dw = date('w',strtotime('+2 day'));
                                            switch ($dw) {
                                                case 0:
                                                echo "Niedziela";
                                                break;
                                                case 1:
                                                echo "Poniedziałek";
                                                break;
                                                case 2:
                                                echo "Wtorek";
                                                break;
                                                case 3:
                                                echo "Środa";
                                                break;
                                                case 4:
                                                echo "Czwartek";
                                                break;
                                                case 5:
                                                echo "Piątek";
                                                break;
                                                case 6:
                                                echo "Sobota";
                                                break;
                                        }?></div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <?php
                                    $date = date('Y-m-d',strtotime('+2 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo'<img src="images/icons/'.$condition->overall.'.svg" alt="" width=48>';
                                    }?>
                                </div>
                                <div class="degree"><?php
                                    $date = date('Y-m-d',strtotime('+2 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmax;
                                    }
                                    ?><sup>o</sup>C</div>
                                <small><?php
                                    $date = date('Y-m-d',strtotime('+2 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmin;
                                    }
                                    ?><sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day"><?php
                                    $dw = date('w',strtotime('+3 day'));
                                            switch ($dw) {
                                                case 0:
                                                echo "Niedziela";
                                                break;
                                                case 1:
                                                echo "Poniedziałek";
                                                break;
                                                case 2:
                                                echo "Wtorek";
                                                break;
                                                case 3:
                                                echo "Środa";
                                                break;
                                                case 4:
                                                echo "Czwartek";
                                                break;
                                                case 5:
                                                echo "Piątek";
                                                break;
                                                case 6:
                                                echo "Sobota";
                                                break;
                                        }?></div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                <?php
                                    $date = date('Y-m-d',strtotime('+3 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo'<img src="images/icons/'.$condition->overall.'.svg" alt="" width=48>';
                                    }?>
                                    
                                </div>
                                <div class="degree"><?php
                                    $date = date('Y-m-d',strtotime('+3 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmax;
                                    }
                                    ?><sup>o</sup>C</div>
                                <small><?php
                                    $date = date('Y-m-d',strtotime('+3 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmin;
                                    }
                                    ?><sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day"><?php
                                    $dw = date('w',strtotime('+4 day'));
                                            switch ($dw) {
                                                case 0:
                                                echo "Niedziela";
                                                break;
                                                case 1:
                                                echo "Poniedziałek";
                                                break;
                                                case 2:
                                                echo "Wtorek";
                                                break;
                                                case 3:
                                                echo "Środa";
                                                break;
                                                case 4:
                                                echo "Czwartek";
                                                break;
                                                case 5:
                                                echo "Piątek";
                                                break;
                                                case 6:
                                                echo "Sobota";
                                                break;
                                        }?></div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <?php
                                    $date = date('Y-m-d',strtotime('+4 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo'<img src="images/icons/'.$condition->overall.'.svg" alt="" width=48>';
                                    }?>
                                </div>
                                <div class="degree"><?php
                                    $date = date('Y-m-d',strtotime('+4 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmax;
                                    }
                                    ?><sup>o</sup>C</div>
                                <small><?php
                                    $date = date('Y-m-d',strtotime('+4 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmin;
                                    }
                                    ?><sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day"><?php
                                    $dw = date('w',strtotime('+5 day'));
                                            switch ($dw) {
                                                case 0:
                                                echo "Niedziela";
                                                break;
                                                case 1:
                                                echo "Poniedziałek";
                                                break;
                                                case 2:
                                                echo "Wtorek";
                                                break;
                                                case 3:
                                                echo "Środa";
                                                break;
                                                case 4:
                                                echo "Czwartek";
                                                break;
                                                case 5:
                                                echo "Piątek";
                                                break;
                                                case 6:
                                                echo "Sobota";
                                                break;
                                        }?></div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <?php
                                    $date = date('Y-m-d',strtotime('+5 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo'<img src="images/icons/'.$condition->overall.'.svg" alt="" width=48>';
                                    }?>
                                </div>
                                <div class="degree"><?php
                                    $date = date('Y-m-d',strtotime('+5 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmax;
                                    }
                                    ?><sup>o</sup>C</div>
                                <small><?php
                                    $date = date('Y-m-d',strtotime('+5 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmin;
                                    }
                                    ?><sup>o</sup></small>
                            </div>
                        </div>
                        <div class="forecast">
                            <div class="forecast-header">
                                <div class="day"><?php
                                    $dw = date('w',strtotime('+6 day'));
                                            switch ($dw) {
                                                case 0:
                                                echo "Niedziela";
                                                break;
                                                case 1:
                                                echo "Poniedziałek";
                                                break;
                                                case 2:
                                                echo "Wtorek";
                                                break;
                                                case 3:
                                                echo "Środa";
                                                break;
                                                case 4:
                                                echo "Czwartek";
                                                break;
                                                case 5:
                                                echo "Piątek";
                                                break;
                                                case 6:
                                                echo "Sobota";
                                                break;
                                        }?></div>
                            </div> <!-- .forecast-header -->
                            <div class="forecast-content">
                                <div class="forecast-icon">
                                    <?php
                                    $date = date('Y-m-d',strtotime('+6 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo'<img src="images/icons/'.$condition->overall.'.svg" alt="" width=48>';
                                    }?>
                                </div>
                                <div class="degree"><?php
                                    $date = date('Y-m-d',strtotime('+6 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmax;
                                    }
                                    ?><sup>o</sup>C</div>
                                <small><?php
                                    $date = date('Y-m-d',strtotime('+6 day'));
                                    foreach($conditions as $condition){
                                        if($condition->date == $date)
                                            echo$condition->tempmin;
                                    }
                                    ?><sup>o</sup></small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <main class="main-content">


                 <div id='status'></div>
    <div id='map'></div>
<?php 
//if (!isset($_GET['location']))
//    $user = App\User::find($_GET['location']);
 //   echo $user->id;
?>
   <script>
      if (typeof navigator.geolocation == 'undefined')
         alert("Funkcja geolokacji nie jest obsługiwana.")
      else
        navigator.geolocation.getCurrentPosition(granted, denied)

      function granted(position)
      {
        // UsuĹ poniĹźsze komentarze, aby wyĹwietliÄ lokalizacjÄ
        //    alert("JesteĹ tutaj:\n"
        //       + position.coords.latitude + ", "
        //       + position.coords.longitude)
 
        O('status').innerHTML = 'Pozwolenie przyznane'
        S('map').border       = '1px solid black'
        S('map').width        = '60%'
        S('map').height       = '700px'
        S('map').margin       = 'auto'
        O('status').margin = '0'
        var lat   = position.coords.latitude
        var long  = position.coords.longitude
        var gmap  = O('map')
        var myLatLng = {lat: lat, lng: long};
 
        var gopts =
        {
          center: new google.maps.LatLng(lat, long),
          zoom: 9, mapTypeId: google.maps.MapTypeId.ROADMAP
 
        }
        var map = new google.maps.Map(gmap, gopts)
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title:'Ok'
        });
      }

      function denied(error)
      {
        var message

        switch(error.code)
        {
          case 1: message = 'Brak pozwolenia'; break;
          case 2: message = 'Położenie niedostępne'; break;
          case 3: message = 'Przekroczony czas operacji'; break;
          case 4: message = 'Nieznany błąd'; break;
        }

        // UsuĹ poniĹźsze komentarze, aby wyĹwietliÄ lokalizacjÄ
        //   alert("BĹÄd w ustalaniu poĹoĹźenia: " + message)

        O('status').innerHTML = message
      }
    </script>
                </div>

                <div class="fullwidth-block" data-bg-color="#4c4e5a">
                    <div class="container">
                    <h2 class="section-title">Najnowsze wiadomości</h2>
                        <div class="row" >
                        @foreach($articles as $article)
                            <div class="col-md-4">
                                <div class="news">
                                    <div class="photo-grid">
                                    <a href="#"><img src="images/thumb-1.jpg" alt="#"></a>
                                    </div>
                                    <h3><a href="#">{{$article->title}}</a></h3>
                                    <p><?php
                                    $text = $article->body;
                                    $maxPos = 255;
                                    if(strlen($text) > $maxPos){
                                        $lastPos = ($maxPos -3)-strlen($text);
                                        $text = substr($text,0,strrpos($text,' ',$lastPos)).'...';
                                    }echo $text;
                                    ?></p>
                                    <small class="date">8 oct, 8:00AM</small>
                                </div>
                            </div>
                            @endforeach
                            
                        </div>
                    </div>
                    

            </main> <!-- .main-content -->

            <footer class="site-footer">
            <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <h2 class="section-title">Mapa serwisu</h2>
                                <ul class="arrow-list">
                                    <li><a href="main">Prognoza</a></li>
                                    <li><a href="the_news">Wiadomości</a></li>
                                    <li><a href="contact">Kontakt</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h2 class="section-title">Wiadomości</h2>
                                <ul class="arrow-list">
                                    @foreach($categories as $category)
                                        <li><a href="kategorie/{{$category->category}}">{{$category->category}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <h2 class="section-title"></h2>
                                <form action="#" class="subscribe-form">
                                <input type="text" placeholder="Wpisz swój mail...">
                                <input type="submit" value="Subskrybuj">
                            </form>

                            </div>
                            <div class="col-md-3 col-md-offset-1">
                            <div class="social-links">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                            </div>
                        </div>
                        </div>
                    </div>
                <div class="container">
                    <p class="colophon">Copyright 2016 Zespół 404. All rights reserved</p>
                </div>
            </footer> <!-- .site-footer -->
        </div>

        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/app.js"></script>

    </body>

</html>

